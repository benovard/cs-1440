//
// Created by Ben Ovard on 4/19/17.
//

#include "Configuration.h"

void Configuration::addConfig(string key, string value) {
    m_configs.push_back(KeyValue<string,string>(key, value));
}

string Configuration::valueAsString(string key) {
    for (int i = 0; i < m_configs.size(); i++){
        if(m_configs[i].getKey() == key){
            return m_configs[i].getValue();
        }
    }
}

int Configuration::valueAsInt(string key) {
    for (int i = 0; i < m_configs.size(); i++){
        if(m_configs[i].getKey() == key){
            string input = m_configs[i].getValue();
            return convertStringToInt(input);
        }
    }
}

double Configuration::valueAsDouble(string key) {
    for (int i = 0; i < m_configs.size(); i++){
        if(m_configs[i].getKey() == key){
            string input = m_configs[i].getValue();
            return convertStringToDouble(input);
        }
    }
}
