//
// Created by Ben Ovard on 4/19/17.
//

#include "DenialOfServiceAnalyzer.h"
#include <string>
#include <fstream>
#include <vector>
using namespace std;

ResultSet DenialOfServiceAnalyzer::run(ifstream& fin) {

    while(!fin.eof()){

        string time;
        string srcAddress;
        string srcPort;
        string desPort;

        getline(fin, time, ',');
        getline(fin, srcAddress, ',');
        getline(fin, srcPort, ',');
        getline(fin, desPort);

        KeyValue<string, Dictionary<string, int>> tempDic;
        if(m_dictionary.getByKeyBool(srcAddress) == true){
            tempDic = m_dictionary.getByKey(srcAddress);

            Dictionary<string, int> tempKey = tempDic.getValue();
            if(tempKey.getByKeyBool(time) == true){
                KeyValue<string, int> otherTemp = tempKey.getByKey(time);
                otherTemp.setValue(otherTemp.getValue() + 1);
            }
            else{
                tempKey.add(time, 1);
            }
        }
        else{
            Dictionary<string, int> d;
            d.add(time, 0);
            m_dictionary.add(srcAddress, d);
        }

    }

    ResultSet results;
    vector<string> temp;
    results.m_results.add("Likely Attackers", temp);
    results.m_results.add("Possible Attackers", temp);
    results.m_results.add("Attack Periods", temp);
    results.m_results.add("Timeframe", temp);

    int timeframe = m_configParameters.valueAsInt("Timeframe");
    int likelyThreshold = m_configParameters.valueAsInt("Likely Attack Message Count");
    int possibleThreshold = m_configParameters.valueAsInt("Possible Attack Message Count");

    for(int i = 0; i < m_dictionary.getSize(); i++){

    }

    return results;
}