//
// Created by Ben Ovard on 4/4/17.
//

#ifndef GENERICDICTIONARY_DICTIONARY_H
#define GENERICDICTIONARY_DICTIONARY_H

#include "KeyValue.h"
#include <vector>
#include <iostream>
using namespace std;

template <typename T, typename G>
class Dictionary {

private:
    vector<KeyValue<T,G>> m_dictionary;
    int m_size = 0;
    int m_keyValueCount = 0;

public:

    Dictionary() {

        for(int i = 0; i < m_size; i++){
            m_dictionary.push_back(KeyValue<T, G>());
        }
    }


    Dictionary(int size) {
        m_size = size;

        for(int i = 0; i < m_size; i++){
            m_dictionary.push_back(KeyValue<T, G>());
        }
    }

//Copy constructor
    Dictionary(const Dictionary &d1) {
        m_dictionary = d1.m_dictionary;
        m_size = d1.m_size;
        m_keyValueCount = d1.m_keyValueCount;
    }


    void add(T key, G value) {

        if(m_keyValueCount == m_dictionary.size()){
            m_dictionary.push_back(KeyValue<T, G>());
            m_size++;
        }

        m_dictionary[m_keyValueCount].setKey(key);
        m_dictionary[m_keyValueCount].setValue(value);
        m_keyValueCount++;
    }

    bool getByKeyBool(T key) {
        for(int i = 0; i < m_size; i++){

            if(m_dictionary[i].getKey() == key){
                return true;
            }
            else{
                return false;
            }
        }
    }

    KeyValue<T, G> getByKey(T key) {

        for(int i = 0; i < m_size; i++){

            if(m_dictionary[i].getKey() == key){
                return m_dictionary[i];
            }
        }
        throw std::invalid_argument("Key not found");
    }


    KeyValue<T, G> getByIndex(int index) {

        if(index < 0 || index > m_size - 1){
            throw string("Invalid index");
        }

        return m_dictionary[index];
    }


    void removeByKey(T key) {

        for(int i = 0; i < m_size; i++){
            if(m_dictionary[i].getKey() == key){
                m_dictionary.erase(m_dictionary.begin() + i);
                m_dictionary.push_back(KeyValue<T, G>());
                m_keyValueCount--;
                return;
            }
        }
        throw string("Key not found");
    }


    void removeByIndex(int index) {

        if(index < 0 || index > m_size - 1){
            throw string("Invalid index");
        }

        m_dictionary.erase(m_dictionary.begin() + index);
        m_dictionary.push_back(KeyValue<T, G>());
        m_keyValueCount--;
    }


    int getCount(){
        return m_keyValueCount;
    }

    int getSize(){return m_size;}
};


#endif //GENERICDICTIONARY_DICTIONARY_H
