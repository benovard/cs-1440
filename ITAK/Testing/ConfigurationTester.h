//
// Created by Ben Ovard on 4/21/17.
//

#ifndef ITAK_CONFIGURATIONTESTER_H
#define ITAK_CONFIGURATIONTESTER_H

#include "../Configuration.h"
#include <iostream>
using namespace std;

class ConfigurationTester {

public:
    void testAdd();
    void testGetAsString();
    void testGetAsInt();
    void testGetAsDouble();
};


#endif //ITAK_CONFIGURATIONTESTER_H
