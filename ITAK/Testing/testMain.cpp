
#include "ConfigurationTester.h"
#include "DosAnalyzerTester.h"
#include "PortScanAnalyzerTester.h"
#include "ResultSetTester.h"

#include <iostream>
using namespace std;

int main(){

    ConfigurationTester configTester;
    configTester.testAdd();
    configTester.testGetAsString();
    configTester.testGetAsInt();
    configTester.testGetAsDouble();

    ResultSetTester rsTester;
    rsTester.testConstructor();
    rsTester.testPrint();

    DosAnalyzerTester dosTester;
    dosTester.testRun();

    PortScanAnalyzerTester portTester;
    portTester.testRun();

}