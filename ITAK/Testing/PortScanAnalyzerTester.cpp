//
// Created by Ben Ovard on 4/21/17.
//

#include "PortScanAnalyzerTester.h"

void PortScanAnalyzerTester::testRun() {

    cout << "Executing PortScanAnalyzerTester::testRun()" <<endl;

    ifstream fin("../SampleData.csv");
    if(!fin){
        cout << "Error opening file" <<endl;
        return;
    }

    PortScanAnalyzer myPortAnalyzer;
    myPortAnalyzer.run(fin);
}