//
// Created by Ben Ovard on 4/21/17.
//

#ifndef ITAK_DOSANALYZERTESTER_H
#define ITAK_DOSANALYZERTESTER_H

#include "../DenialOfServiceAnalyzer.h"

class DosAnalyzerTester {

public:
    void testRun();

};


#endif //ITAK_DOSANALYZERTESTER_H
