//
// Created by Ben Ovard on 4/21/17.
//

#include "ResultSetTester.h"
#include <iostream>
using namespace std;

void ResultSetTester::testConstructor() {

    cout << "Exectuing ResultSetTester::testConstructor()" <<endl;

    ResultSet results;
    vector<string> empty;
    results.m_results.add("Tester", empty);

    if(results.m_results.getByIndex(0).getKey() != "Tester"){
        cout << "Failure in constructing ResultSet" << endl;
    }
}

void ResultSetTester::testPrint() {

    cout << "Executing ResultSetTester::testPrint()" <<endl;

    ResultSet results;
    vector<string> v1;
    v1.push_back("10");
    v1.push_back("20");
    v1.push_back("30");
    results.m_results.add("Test", v1);
    results.print(cout);
}


