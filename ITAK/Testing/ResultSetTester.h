//
// Created by Ben Ovard on 4/21/17.
//

#ifndef ITAK_RESULTSETTESTER_H
#define ITAK_RESULTSETTESTER_H

#include "../ResultSet.h"

class ResultSetTester {

public:
    void testConstructor();
    void testPrint();

};


#endif //ITAK_RESULTSETTESTER_H
