//
// Created by Ben Ovard on 4/21/17.
//

#include "ConfigurationTester.h"

void ConfigurationTester::testAdd() {

    cout << "Executing ConfigurationTester::testAdd()" <<endl;

    Configuration myConfig;
    myConfig.addConfig("Test Parameter", "20");

    if(myConfig.m_configs[0].getKey() != "Test Parameter" || myConfig.m_configs[0].getValue() != "20"){
        cout << "Error in adding configuration to myConfig" << endl;
    }

}

void ConfigurationTester::testGetAsString() {

    cout << "Executing ConfigurationTester::testGetAsString()" <<endl;

    Configuration myConfig;
    myConfig.addConfig("Test Parameter", "593");
    myConfig.addConfig("Other Parameter", "10000");

    if(myConfig.valueAsString("Other Parameter") != "10000"){
        cout << "Error in returning value as a string" << endl;
    }
}

void ConfigurationTester::testGetAsInt() {

    cout << "Executing ConfigurationTester::testGetAsInt()" <<endl;

    Configuration myConfig;
    myConfig.addConfig("Test Parameter", "593");
    myConfig.addConfig("Other Parameter", "10000");

    if(myConfig.valueAsInt("Test Parameter") != 593){
        cout << "Error in returning value as an int" << endl;
    }
}

void ConfigurationTester::testGetAsDouble() {

    cout << "Executing ConfigurationTester::testGetAsDouble()" <<endl;

    Configuration myConfig;
    myConfig.addConfig("Test Parameter", "593");
    myConfig.addConfig("Other Parameter", "683.33");

    if(myConfig.valueAsDouble("Other Parameter") != 683.33){
        cout << "Error in returning value as a double" << endl;
    }

}