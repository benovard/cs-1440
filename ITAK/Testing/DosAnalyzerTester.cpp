//
// Created by Ben Ovard on 4/21/17.
//

#include "DosAnalyzerTester.h"

void DosAnalyzerTester::testRun() {

    cout << "Executing DosAnalyzerTester::testRun()" <<endl;

    ifstream fin("../SampleData.csv");
    if(!fin){
        cout << "Error opening file" <<endl;
        return;
    }

    DenialOfServiceAnalyzer myDosAnalyzer;
    myDosAnalyzer.run(fin);


}