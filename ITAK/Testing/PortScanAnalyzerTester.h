//
// Created by Ben Ovard on 4/21/17.
//

#ifndef ITAK_PORTSCANANALYZERTESTER_H
#define ITAK_PORTSCANANALYZERTESTER_H

#include "../PortScanAnalyzer.h"


class PortScanAnalyzerTester {

public:
    void testRun();

};


#endif //ITAK_PORTSCANANALYZERTESTER_H
