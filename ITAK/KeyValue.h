//
// Created by Ben Ovard on 4/19/17.
//

#ifndef ITAK_KEYVALUE_H
#define ITAK_KEYVALUE_H

template <typename T, typename G>
class KeyValue {

private:
    T m_key;
    G m_value;

public:
    KeyValue() {

    }

    KeyValue(T key, G value) {
        m_key = key;
        m_value = value;
    }

    KeyValue(const KeyValue& kv) {
        m_key = kv.m_key;
        m_value = kv.m_value;
    }

    T getKey(){return m_key;}
    G getValue(){return m_value;}

    void setKey(T key){m_key = key;}
    void setValue(G value){m_value = value;}
};



#endif //ITAK_KEYVALUE_H
