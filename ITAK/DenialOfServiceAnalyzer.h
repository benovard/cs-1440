//
// Created by Ben Ovard on 4/19/17.
//

#ifndef ITAK_DENIALOFSERVICEANALYZER_H
#define ITAK_DENIALOFSERVICEANALYZER_H

#include "Analyzer.h"
#include "Dictionary.h"

class DenialOfServiceAnalyzer: public Analyzer {

private:
    Dictionary<string,Dictionary<string, int>> m_dictionary;

public:
    ResultSet run(ifstream& fin);

};


#endif //ITAK_DENIALOFSERVICEANALYZER_H
