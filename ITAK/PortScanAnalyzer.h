//
// Created by Ben Ovard on 4/19/17.
//

#ifndef ITAK_PORTSCANANALYZER_H
#define ITAK_PORTSCANANALYZER_H

#include "Analyzer.h"

class PortScanAnalyzer: public Analyzer {

private:
    Dictionary<string, vector<int>> m_dictionary;

public:
    ResultSet run(ifstream& fin);

};


#endif //ITAK_PORTSCANANALYZER_H
