#include <iostream>
#include <fstream>
#include "DenialOfServiceAnalyzer.h"
#include "PortScanAnalyzer.h"
#include "ResultSet.h"

using namespace std;

int main() {

    ifstream fin("../SampleData.csv");
    if(!fin){
        cout << "Error opening file" <<endl;
        return 1;
    }

    DenialOfServiceAnalyzer myDosAnalyzer;
    myDosAnalyzer.m_configParameters.addConfig("Timeframe", "200");
    myDosAnalyzer.m_configParameters.addConfig("Likely Attack Message Count", "20");
    myDosAnalyzer.m_configParameters.addConfig("Possible Attack Message Count", "15");
    ResultSet results = myDosAnalyzer.run(fin);
    cout << "DOS analysis results: " <<endl;
    results.print(cout);
    cout << endl;

    fin.close();


    ifstream input("../SampleData.csv");
    if(!input){
        cout << "Error opening file" <<endl;
        return 1;
    }

    PortScanAnalyzer myPortAnalyzer;
    myPortAnalyzer.m_configParameters.addConfig("Likely Attack Port Count", "20");
    myPortAnalyzer.m_configParameters.addConfig("Possible Attack Port Count", "15");
    ResultSet results2 = myPortAnalyzer.run(input);
    cout << "Port Scan analysis results: " <<endl;
    results2.print(cout);
    cout << endl;

    input.close();

    return 0;
}