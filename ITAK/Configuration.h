//
// Created by Ben Ovard on 4/19/17.
//

#ifndef ITAK_CONFIGURATION_H
#define ITAK_CONFIGURATION_H

#include "KeyValue.h"
#include "Utils.h"
#include <vector>
using namespace std;

class Configuration {

public:
    vector<KeyValue<string,string>> m_configs;
    void addConfig(string key, string value);
    string valueAsString(string key);
    int valueAsInt(string key);
    double valueAsDouble(string key);
};


#endif //ITAK_CONFIGURATION_H
