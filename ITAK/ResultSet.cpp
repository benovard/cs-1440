//
// Created by Ben Ovard on 4/19/17.
//

#include "ResultSet.h"

ResultSet::ResultSet() {

}

void ResultSet::print(ostream &out) {


    for(int i = 0; i < m_results.getSize(); i++){
        vector<string> temp = m_results.getByIndex(i).getValue();
        out << m_results.getByIndex(i).getKey() << ": ";
        for(int j = 0; j < temp.size(); j++){
            out << temp[j] << ", ";
        }
        out << endl;
    }

}
