//
// Created by Ben Ovard on 4/19/17.
//

#ifndef ITAK_RESULTSET_H
#define ITAK_RESULTSET_H

#include "Dictionary.h"
#include "KeyValue.h"
#include <fstream>
#include <vector>
#include <string>
using namespace std;

class ResultSet {

private:

public:
    Dictionary<string, vector<string>> m_results;
    ResultSet();
    void print(ostream& out);
};


#endif //ITAK_RESULTSET_H
