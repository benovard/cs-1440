//
// Created by Ben Ovard on 4/19/17.
//

#ifndef ITAK_ANALYZER_H
#define ITAK_ANALYZER_H

#include "ResultSet.h"
#include "Configuration.h"
#include <fstream>
using namespace std;

class Analyzer {

private:

public:
    virtual ResultSet run(ifstream& fin) = 0;
    Analyzer(Configuration config);
    Analyzer();
    Configuration m_configParameters;

};


#endif //ITAK_ANALYZER_H
