//
// Created by Ben Ovard on 4/19/17.
//

#include "PortScanAnalyzer.h"

ResultSet PortScanAnalyzer::run(ifstream& fin) {

    while(!fin.eof()){

        string time;
        string srcAddress;
        string srcPort;
        string tempDes;

        getline(fin, time, ',');
        getline(fin, srcAddress, ',');
        getline(fin, srcPort, ',');
        getline(fin, tempDes);

        int desPort = convertStringToInt(tempDes);

        KeyValue<string, vector<int>> tempDic;
        if(m_dictionary.getByKeyBool(srcAddress) == true){
            tempDic = m_dictionary.getByKey(srcAddress);

            vector<int> tempKey = tempDic.getValue();
            for (auto i : tempKey) {
                if (tempKey[i] == desPort) {

                } else {
                    tempKey.push_back(desPort);
                }
            }
        }
        else{
            vector<int> temp;
            m_dictionary.add(srcAddress, temp);
        }

    }

    ResultSet results;
    vector<string> temp;
    results.m_results.add("Likely Attackers", temp);
    results.m_results.add("Possible Attackers", temp);
    results.m_results.add("Port Count", temp);

    int likelyThreshold = m_configParameters.valueAsInt("Likely Attack Message Count");
    int possibleThreshold = m_configParameters.valueAsInt("Possible Attack Message Count");

    for(int i = 0; i < m_dictionary.getSize(); i++){

        KeyValue<string, vector<int>> src = m_dictionary.getByIndex(i);
        if(src.getValue().size() >= likelyThreshold){
            results.m_results.getByKey("Likely Attackers").getValue().push_back(src.getKey());
        }
        if(src.getValue().size() >= possibleThreshold){
            results.m_results.getByKey("Possible Attackers").getValue().push_back(src.getKey());
        }
    }

    return results;
}