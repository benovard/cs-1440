//
// Created by Stephen Clyde on 2/16/17.
//

#include "Deck.h"
#include <vector>
#include <iostream>

Deck::Deck(int cardSize, int cardCount, int numberMax)
{
    m_deck = new Card*[cardCount];
    m_cardCount = cardCount;

    for(int i = 0; i < cardCount; i++){
        m_deck[i] = new Card(cardSize, numberMax);
    }
}

Deck::~Deck()
{
    delete[] m_deck;
}

void Deck::print(std::ostream& out) const
{
    for(int i = 0; i < m_cardCount; i++){
        m_deck[i]->print(out, i+1);
    }
}

void Deck::print(std::ostream& out, int cardIndex) const
{
    m_deck[cardIndex - 1]->print(out, cardIndex);
}

int Deck::getCardCount() {
    return m_cardCount;
}



