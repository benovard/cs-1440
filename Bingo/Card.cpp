//
// Created by Ben Ovard on 2/23/17.
//

#include <cstdlib>
#include <iomanip>
#include "Card.h"
#include <vector>
using namespace std;

Card::Card(int size, int max) {

    m_cardSize = size;
    m_maxNumber = max;
    m_data = new int*[size];

    vector<int> numbers;
    for(int i = 1; i <= max; i++){
        numbers.push_back(i);
    }
    std::random_shuffle(numbers.begin(), numbers.end());

    int count = 0;
    for(int i = 0; i < size; i++){

        m_data[i] = new int[size];

        for(int j = 0; j < size; j++){

            m_data[i][j] = numbers[count];
            count++;
        }
    }
}

Card::Card() {

}

Card::~Card() {

    for(int i = 0; i < m_cardSize; i++){

        delete  m_data[i];
    }
    delete[] m_data;
    m_data = nullptr;
}

void Card::print(std::ostream &out, int index) {

    out << "Card #" << index << endl;

    for(int i = 0; i < m_cardSize; i++) {

        for (int j = 0; j < m_cardSize; j++) {
            out << "+----";
        }
        out << "+" << endl;

        for (int j = 0; j < m_cardSize; j++) {
            out << "| " <<setw(2) << m_data[i][j] << " ";
        }
        out << "|" << endl;
    }

    for (int j = 0; j < m_cardSize; j++) {
        out << "+----";
    }
    out << "+" <<endl <<endl;
}

int Card::getCardSize() {
    return m_cardSize;
}

int Card::getMaxNumber() {
    return m_maxNumber;
}
