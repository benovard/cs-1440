//
// Created by Ben Ovard on 2/23/17.
//

#ifndef BINGO_CARD_H
#define BINGO_CARD_H

#include <ostream>

class Card {

private:
    int m_cardSize;
    int m_maxNumber;
    int** m_data = nullptr;

public:
    Card(int size, int max);
    Card();
    ~Card();
    void print(std::ostream& out, int index);
    int getCardSize();
    int getMaxNumber();
};


#endif //BINGO_CARD_H
