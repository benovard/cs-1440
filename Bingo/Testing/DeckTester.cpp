//
// Created by Ben Ovard on 2/28/17.
//

#include "DeckTester.h"
#include "../Deck.h"
#include <iostream>
using namespace std;

void DeckTester::testConstructor(){

    cout << "\nTest Suite: DeckTester::testConstructor" <<endl;

    Deck deck(5,5,50);

    cout << "Test case 1" <<endl;
    if(deck.getCardCount() != 5){
        cout << "Failure in construct deck. Unexpected card count of " <<deck.getCardCount() <<endl;
        return;
    }
}