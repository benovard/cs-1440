//
// Created by Ben Ovard on 2/28/17.
//

#include "CardTester.h"
#include "../Card.h"
#include <iostream>
using namespace std;

void CardTester::testConstructor() {

    cout << "\nTest Suite: CardTester::testConstructor" <<endl;

    Card card(5, 50);

    cout << "Test case 1" <<endl;
    if(card.getCardSize() != 5){
        cout << "Failure in construct Card: unexpected size of " << card.getCardSize() <<endl;
        return;
    }

    cout << "Test case 2" <<endl;
    if(card.getMaxNumber() != 50){
        cout << "Failure in construct Card: unexpected max number of " << card.getMaxNumber() <<endl;
        return;
    }
}