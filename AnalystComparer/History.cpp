//
// Created by Ben Ovard on 2/1/17.
//

#include "History.h"
#include <fstream>

int History::getSimulationDays(){
    return m_simulationDays;
}

int History::getInitialMoney(){
    return m_seedMoney;
}

int History::load(std::ifstream& fin) {
    fin >> m_simulationDays;
    fin >> m_seedMoney;
    fin >> m_purchaseSalesCount;

    int i = 0;
    std::string temp;

    m_purchaseSales[i]->PurchaseSale(fin);

}