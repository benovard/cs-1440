//
// Created by Ben Ovard on 2/1/17.
//

#include "Analyst.h"
#include <fstream>

std::string Analyst::getName(){
    return m_name;
}

std::string Analyst::getInitials(){
    return m_initials;
}

History & Analyst::getHistory(){
    return m_history;
}

int Analyst::load(std::ifstream& fin){

    std::getline(fin, m_name);
    std::getline(fin, m_initials);
    m_history.load(fin);
}