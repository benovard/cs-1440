//
// Created by Ben Ovard on 2/1/17.
//

#include "PurchaseSale.h"
#include "Utils.h"

std::string PurchaseSale::getSymbol(){
    return m_symbol;
}

int PurchaseSale::getPurchaseDateTime(){
    return m_purchaseDateTime;
}

int PurchaseSale::getSaleDateTime(){
    return m_saleDateTime;
}

PurchaseSale::PurchaseSale(std::ifstream fin) {
    int expectedElements = 8;
    std::string elements[expectedElements];
    std::string temp;
    fin >> temp;
    split(temp, ',', elements, expectedElements);


}