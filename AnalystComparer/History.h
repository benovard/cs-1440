//
// Created by Ben Ovard on 2/1/17.
//

#ifndef ANALYSTCOMPARER_HISTORY_H
#define ANALYSTCOMPARER_HISTORY_H

#include "PurchaseSale.h"

class History {

private:
    int m_simulationDays;
    int m_seedMoney;
    PurchaseSale** m_purchaseSales;
    int m_purchaseSalesCount;
    int m_currentPurchaseSale;

public:
    int getSimulationDays();
    int getInitialMoney();
    int computeTotalProfitLoss();
    int computeProfitLossPerDay();
    void resetIteration();
    PurchaseSale* nextPurchaseSale();
    int load(std::ifstream&);

};


#endif //ANALYSTCOMPARER_HISTORY_H
