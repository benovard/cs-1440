//
// Created by Ben Ovard on 2/1/17.
//

#ifndef ANALYSTCOMPARER_ANALYST_H
#define ANALYSTCOMPARER_ANALYST_H

#include <string>
#include "History.h"

class Analyst {

private:
    std::string m_name;
    std::string m_initials;
    History m_history;

public:
    std::string getName();
    std::string getInitials();
    History & getHistory();
    int load(std::ifstream&);
    float getStockPerformance(std::string);

};


#endif //ANALYSTCOMPARER_ANALYST_H
