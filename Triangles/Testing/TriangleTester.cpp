//
// Created by Stephen Clyde on 2/3/17.
//

#include "TriangleTester.h"

#include <iostream>
#include <cmath>

#include "../Triangle.h"
#include "../Utils.h"

void TriangleTester::testFirstConstructor()
{
    std::cout << "Execute TriangleTester::testConstructorWithStrings" << std::endl;

    Point** points = new Point*[3];
    points[0] = new Point(1,2,8);
    points[1] = new Point(3,1,4);
    points[2] = new Point(6,4,3);

    Triangle t(points);
    if (!t.isValid())
    {
        std::cout << "Failure to constructor a triangle from 3 points; unexpectedly invalid" << std::endl;
        return;
    }

    if (!t.isTriangle())
    {
        std::cout << "Failure to constructor a triangle from 3 points; unexpectedly not a triangle" << std::endl;
        return;
    }

    if (t.getTriangleType()!='S')
    {
        std::cout << "Triangle: unexpected type of "
                 << t.getTriangleType() << std::endl;
        return;
    }

    if (!approximatelyEquals(t.computerArea(), 9.35414, 0.001))
    {
        std::cout << "Triangle: unexpected area of "
                 << t.computerArea() << std::endl;
        return;
    }

    const Point* p = t.getPoint(0);
    if (p== nullptr)
    {
        std::cout << "Failure to get point 0 from triangle" << std::endl;
        return;
    }

    if (p->getX()!=1 || p->getY()!=2 || p->getZ()!=8)
    {
        std::cout << "Point 0 of triangle is not as expected, namely (1,2,8)" << std::endl;
        return;
    }

    p = t.getPoint(1);
    if (p== nullptr)
    {
        std::cout << "Failure to get point 1 from triangle" << std::endl;
        return;
    }

    if (p->getX()!=3 || p->getY()!=1 || p->getZ()!=4)
    {
        std::cout << "Point 1 of triangle is not as expected, namely (3,1,4)" << std::endl;
        return;
    }

    p = t.getPoint(2);
    if (p== nullptr)
    {
        std::cout << "Failure to get point 1 from triangle" << std::endl;
        return;
    }

    if (p->getX()!=6 || p->getY()!=4 || p->getZ()!=3)
    {
        std::cout << "Point 2 of triangle is not as expected, namely (6,4,3)" << std::endl;
        return;
    }
}

void TriangleTester::testSecondConstructor()
{
    std::cout << "Execute TriangleTester::testSecondConstructor" << std::endl;

    std::string s1 = "1,2,8|3,1,4|6,4,3";
    Triangle t1(s1);

    if (!t1.isValid()){
        std::cout << "Failure to construct t1: unexpectedly invalid" << std::endl;
    }

    if (!t1.isTriangle()){
        std::cout << "Failure to construct t1: unexpectedly not a triangle" << std::endl;
    }

    if (t1.getTriangleType() != 'S'){
        std::cout << "Failure in getTriangleType() unexpected triangle type" <<std::endl;
    }

    if (!approximatelyEquals(t1.computerArea(), 9.35414, 0.001))
    {
        std::cout << "Triangle: unexpected area" <<std::endl;
        return;
    }
}

void TriangleTester::testEquilateralTriangles()
{
    std::cout << "Execute TriangleTester::testEquilateralTriangles" << std::endl;

    std::string triangleStr = "-10,0,0|10,0,0|0,17.32050808,0";
    Triangle t1(triangleStr);
    if (!t1.isValid())
    {
        std::cout << "Failure to constructor a triangle from 3 points; unexpectedly invalid" << std::endl;
        return;
    }

    if (!t1.isTriangle())
    {
        std::cout << "Failure to constructor a triangle from 3 points; unexpectedly not a triangle" << std::endl;
        return;
    }

    if (t1.getTriangleType()!='E')
    {
        std::cout << "Triangle: unexpected type of "
                 << t1.getTriangleType() << std::endl;
        return;
    }

    if (!approximatelyEquals(t1.computerArea(), 173.205, 0.001))
    {
        std::cout << "Triangle: unexpected area of "
                 << t1.computerArea() << std::endl;
        return;
    }

}

void TriangleTester::testIsoscelesTriangles()
{
    std::cout << "Execute TriangleTester::testIsoscelesTriangles" << std::endl;

    std::string s1 = "-2,0,0|2,0,0|0,5,0";
    Triangle t1(s1);

    if (!t1.isValid())
    {
        std::cout << "Failure to construct triangle: unexpectedly invalid" << std::endl;
        return;
    }

    if (!t1.isTriangle())
    {
        std::cout << "Failure to construct triangle: unexpectedly not a triangle" << std::endl;
        return;
    }

    if (t1.getTriangleType()!='I')
    {
        std::cout << "Triangle: unexpected type " <<std::endl;
        return;
    }

    if (!approximatelyEquals(t1.computerArea(), 10, 0.001))
    {
        std::cout << "Triangle: unexpected area of "
                  << t1.computerArea() << std::endl;
        return;
    }

}

void TriangleTester::testScaleneTriangles()
{
    std::cout << "Execute TriangleTester::testScaleneTriangles" << std::endl;

    std::string s1 = "0,0,0|1,0,0|5,10,0";
    Triangle t1(s1);

    if (!t1.isValid())
    {
        std::cout << "Failure to construct triangle: unexpectedly invalid" << std::endl;
        return;
    }

    if (!t1.isTriangle())
    {
        std::cout << "Failure to construct triangle: unexpectedly not a triangle" << std::endl;
        return;
    }

    if (t1.getTriangleType()!='S')
    {
        std::cout << "Triangle: unexpected type of " << std::endl;
        return;
    }

    if (!approximatelyEquals(t1.computerArea(), 4.999, 0.001))
    {
        std::cout << "Triangle: unexpected area of "
                  << t1.computerArea() << std::endl;
        return;
    }
}

void TriangleTester::testNonTriangles()
{
    std::cout << "Execute TriangleTester::testNonTriangles" << std::endl;

    std::string s1 = "0,0,0|0,5,0|0,12,0";
    Triangle t1(s1);

//    if (t1.isValid()){
//        std::cout << "Failure in construct triangle: unexpectedly valid" <<std::endl;
//        return;
//    }

    if (t1.isTriangle()){
        std::cout << "Failure in construct triangle: unexpectedly a triangle" <<std::endl;
        return;
    }

    if (t1.getTriangleType() != 'N'){
        std::cout << "Failure in triangle type: unexpected type" <<std::endl;
    }

    if (t1.computerArea() != 0){
        std::cout << "Failure in triangle: unexpected area" <<std::endl;
    }
}

void TriangleTester::testInvalid()
{
    std::cout << "Execute TriangleTester::testInvalid" << std::endl;

    std::string s1 = "0,0,0|0,0,0|0,12,0";
    Triangle t1(s1);

    if(t1.getPoint(0) == t1.getPoint(1) || t1.getPoint(1) == t1.getPoint(2) || t1.getPoint(0) == t1.getPoint(2) && t1.isTriangle()){
        std::cout <<"Failure in triangle: unexpectedly a triangle" <<std::endl;
    }
}
