#include "KeyValueTester.h"
#include "DictionaryTester.h"

int main(){


    KeyValueTester kvTester;
    kvTester.testFirstConstructor();
    kvTester.testSecondConstructor();
    kvTester.testCopyConstructor();
    kvTester.testGettersAndSetters();

    DictionaryTester dTester;
    dTester.testFirstConstructor();
    dTester.testSecondConstructor();
    dTester.testCopyConstructor();
    dTester.testAddAndGetters();
    dTester.testRemove();

    return 0;
}