//
// Created by Ben Ovard on 4/4/17.
//

#include "DictionaryTester.h"
#include <string>
using namespace std;

void DictionaryTester::testFirstConstructor() {

    cout << "Execute KeyValueTester::testFirstConstructor()" <<endl;

    Dictionary<string, string> d;

    if(d.getCount() != 0 || d.getSize() != 10){
        cout << "Failure in constructing Dictionary" <<endl;
    }
}

void DictionaryTester::testSecondConstructor() {

    cout << "Execute KeyValueTester::testFirstConstructor()" <<endl;

    Dictionary<string, string> d(50);

    if(d.getCount() != 0 || d.getSize() != 50){
        cout << "Failure in constructing Dictionary" <<endl;
    }
}

void DictionaryTester::testCopyConstructor() {

    cout << "Execute KeyValueTester::testCopyConstructor()" <<endl;

    Dictionary<string, string> d1;
    Dictionary<string, string> d2;

    d1.add("milk", "chocolate");
    d1.add("state", "ohio");
    d1.add("bank", "wells fargo");

    d2 = d1;

    if(d2.getCount() != 3){
        cout << "Failure in constructing Dictionary" <<endl;
    }
}

void DictionaryTester::testAddAndGetters() {

    Dictionary<string, string> d1;
    d1.add("milk", "chocolate");
    d1.add("state", "ohio");
    d1.add("bank", "wells fargo");

    if(d1.getByKey("state").getValue() != "ohio"){
        cout <<"Failure in get KeyValue by key" <<endl;
    }

    if(d1.getByIndex(0).getValue() != "chocolate"){
        cout <<"Failure in get KeyValue by index" <<endl;
    }
}

void DictionaryTester::testRemove() {

    Dictionary<string, string> d1;
    d1.add("milk", "chocolate");
    d1.add("state", "ohio");
    d1.add("bank", "wells fargo");

    d1.removeByIndex(0);
    d1.removeByKey("state");

    if(d1.getCount() != 1 || d1.getByIndex(0).getKey() == "milk"){
        cout << "Error in removing KeyValue" <<endl;
    }
}