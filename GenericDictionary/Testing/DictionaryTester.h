//
// Created by Ben Ovard on 4/4/17.
//

#ifndef GENERICDICTIONARY_DICTIONARYTESTER_H
#define GENERICDICTIONARY_DICTIONARYTESTER_H

#include "../Dictionary.h"

class DictionaryTester {

public:
    void testFirstConstructor();
    void testSecondConstructor();
    void testCopyConstructor();
    void testAddAndGetters();
    void testRemove();

};


#endif //GENERICDICTIONARY_DICTIONARYTESTER_H
