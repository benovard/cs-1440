//
// Created by Ben Ovard on 4/4/17.
//

#ifndef GENERICDICTIONARY_KEYVALUETESTER_H
#define GENERICDICTIONARY_KEYVALUETESTER_H

#include "../KeyValue.h"


class KeyValueTester {

public:
    void testFirstConstructor();
    void testSecondConstructor();
    void testCopyConstructor();
    void testGettersAndSetters();
};


#endif //GENERICDICTIONARY_KEYVALUETESTER_H
