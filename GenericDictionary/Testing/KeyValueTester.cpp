//
// Created by Ben Ovard on 4/4/17.
//

#include "KeyValueTester.h"
#include <iostream>
#include <string>
using namespace std;

void KeyValueTester::testFirstConstructor() {

    cout << "Execute KeyValueTester::testFirstConstructor()" <<endl;

    KeyValue<string, string> kv;

    if(kv.getKey() != "" || kv.getValue() != ""){
        cout << "Failure to construct KeyValue" <<endl;
    }
}

void KeyValueTester::testSecondConstructor() {

    cout << "Execute KeyValueTester::testSecondConstructor()" <<endl;

    KeyValue<string, string> kv("cereal", "reese's puffs");

    if(kv.getKey() != "cereal" || kv.getValue() != "reese's puffs"){
        cout << "Failure to construct KeyValue" <<endl;
    }
}

void KeyValueTester::testCopyConstructor() {

    cout << "Execute KeyValueTester::testCopyConstructor()" <<endl;

    KeyValue<string, string> kv1("food", "hot pockets");

    KeyValue<string, string> kv2 = kv1;

    if(kv2.getKey() != "food" || kv2.getValue() != "hot pockets"){
        cout << "Failure to construct KeyValue" <<endl;
    }
}

void KeyValueTester::testGettersAndSetters() {

    cout << "Execute KeyValueTester::testGettersAndSetters()" <<endl;

    KeyValue<string, string> kv;

    kv.setKey("monument");
    kv.setValue("eiffel tower");

    if(kv.getKey() != "monument" || kv.getValue() != "eiffel tower"){
        cout << "Failure to set key and value" <<endl;
    }
}