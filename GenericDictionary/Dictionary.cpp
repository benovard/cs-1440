//
// Created by Ben Ovard on 4/4/17.
//

/*
#include "Dictionary.h"
#include <iostream>
using namespace std;

template <class T, class G>
Dictionary<T, G>::Dictionary() {

    for(int i = 0; i < m_size; i++){
        m_dictionary.push_back(KeyValue<T, G>());
    }
}


template <class T, class G>
Dictionary<T, G>::Dictionary(int size) {
    m_size = size;

    for(int i = 0; i < m_size; i++){
        m_dictionary.push_back(KeyValue<T, G>());
    }
}

//Copy constructor
template <class T, class G>
Dictionary<T, G>::Dictionary(const Dictionary &d1) {
    m_dictionary = d1.m_dictionary;
    m_size = d1.m_size;
    m_keyValueCount = d1.m_keyValueCount;
}


template <class T, class G>
void Dictionary<T, G>::add(T key, G value) {

    if(m_keyValueCount == m_dictionary.size()){
        m_dictionary.push_back(KeyValue<T, G>());
        m_size++;
    }

    m_dictionary[m_keyValueCount].m_key = key;
    m_dictionary[m_keyValueCount].m_value = value;
    m_keyValueCount++;
}

template <class T, class G>
KeyValue<T, G> Dictionary<T, G>::getByKey(T key) {

    for(int i = 0; i < m_size; i++){
        if(m_dictionary[i].m_key == key){
            return m_dictionary[i];
        }
    }
    //TODO: Add exception handling???
}

template <class T, class G>
KeyValue<T, G> Dictionary<T, G>::getByIndex(int index) {

    KeyValue<T, G> temp;
    try {
        temp = m_dictionary[index];
    }
    catch(int e){
        cout << "Invalid index" << endl;
        throw;
    }
    return temp;
}

template <class T, class G>
void Dictionary<T, G>::removeByKey(T key) {

    for(int i = 0; i < m_size; i++){
        if(m_dictionary[i].m_key == key){
            m_dictionary.erase(m_dictionary.begin() + i);
        }
    }

    //TODO: Add exception handling
}

template <class T, class G>
void Dictionary<T, G>::removeByIndex(int index) {

    try {
        m_dictionary.erase(m_dictionary.begin() + index);
    }
    catch(int e){
        cout << "Invalid index" <<endl;
        throw;
    }
}

 */
