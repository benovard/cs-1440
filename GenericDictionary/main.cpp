#include <iostream>
#include "Dictionary.h"

using namespace std;

int main() {

    Dictionary<string, string> d1;
    Dictionary<string, string> d2(50);

    d1.add("hair color", "brown");
    d1.add("hometown", "Seattle");
    d1.add("car", "jeep");
    d1.add("pet", "giraffe");
    d1.add("favorite band", "smashmouth");

    cout << d1.getCount() << " elements in dictionary" << endl;
    KeyValue<string, string> kv;

    try {
         kv = d1.getByKey("car");
         cout << kv.getKey() << ": " << kv.getValue() << endl;
    }
    catch(string e){
        cout << e <<endl;
    }

    KeyValue<string, string> kv2;
    try {
        kv2 = d1.getByIndex(4);
        cout << kv2.getKey() << ": " << kv2.getValue() << endl;
    }
    catch(string e){
        cout << e <<endl;
    }

    d1.removeByKey("hair color");
    d1.removeByIndex(0);

    return 0;
}