cmake_minimum_required(VERSION 3.6)
project(GenericDictionary)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES
            main.cpp
            Dictionary.cpp
            Dictionary.h
            KeyValue.cpp
            KeyValue.h)
add_executable(GenericDictionary ${SOURCE_FILES})

set(TEST_FILES
            Testing/KeyValueTester.cpp
            Testing/KeyValueTester.h
            Testing/DictionaryTester.cpp
            Testing/DictionaryTester.h)
add_executable(Test Testing/testMain.cpp ${TEST_FILES} ${SOURCE_FILES} )